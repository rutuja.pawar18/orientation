**Git** is the way we manage code and there is no software development without git.  
Its a _version-control_ software that makes collaboration with teammates super simple.

>Required Vocabulary  

1. **Repository:**  
 A repository is the collection of files and folders (code files) that you’re using git to track.  
 It’s the big box you and your team throw your code into.  

2. **GitLab:**  
 The 2nd most popular remote storage solution for git repos.

3. **Commit:**  
 When you commit to a repository  
 * it’s like you’re _taking picture/snapshot_ of the files as they exist at that moment.  
 * The commit will only exist on your local machine **until it is pushed to a remote repository.**

4. **Push:**
 Pushing is essentially _syncing_ your commits to [GitLab.](www.gitlab.com5.)

5. **Branch:**  
 >Think of your git repo as a **tree.**  
 * The trunk of the tree, the main software, is called the Master Branch.  
 * The branches of that tree are, well, called branches.  
 * These are separate instances of the code that is different from the main codebase.  

6. **Merge:**
When a branch is free of bugs and ready to become part of the primary codebase, it will get merged into the master branch.  
 **_Merging_** is just what it sounds like: _integrating two branches together._  

7. **Clone:**
 _Cloning_ a repo is pretty much exactly what it sounds like.  
 It takes the entire online repository and makes an exact copy of it on your local machine.

8. **Fork:** 
 _Forking _is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine.  

>Credits  
* [B.J. Keeton](https://git-scm.com/docs/)
* [git-scm page](https://git-scm.com/docs/)