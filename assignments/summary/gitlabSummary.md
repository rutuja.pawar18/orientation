**_Gitlab_**   
![gitlablogo](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS6WIj8kldN2KmE-IuIHDE-tjbBxlXdraMF5g&usqp=CAU)

[Gitlab](www.gitlab.com)

* _**Gitlab**_ is a service that provides remote access to _Git repositories_.  
* In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle.  
* Open source self-hosted Git management software.  
* GitLab offers git repository management, code reviews, issue tracking, activity feeds and wikis.  

>**GitLab Interface**    
![git interface](https://about.gitlab.com/images/blogimages/redesigning-gitlabs-navigation/final.png)  


**_Markdown_**  
**Markdown** is a lightweight and easy-to use syntax for styling all forms of writing on the GitHub Platform. What we learn from _Markdown:_  

* format makes styled collabrative editing easy.  
* differs from traditional formatting approaches.  
* to format text.  

Here are some of the attributes in the module:  
1. **Bold** - To bold text, add two asteriks(*) before and after a word or phrase.  
2. _Italics_ - To italicize text, and underscore(_) before and after a word or phrase.  
3. _**Bold and Italics**_ - To emphasize text with bold and italics at same time, add three asteriks or underscores before and after a word or phrase.  
4. **links**:  
[markup tutorial](https://www.markdowntutorial.com/lesson/1/)
 * [Inline link](https://www.searchenginepeople.com/blog/inline-links.html) - To create an inline link, use set of regular parenthesis immediately after the link text's closing square brackets.  
 * [Reference link][refered link]

 [refered link]: https://referralrock.com/blog/referral-link/
5. **Paragraph** -   
 To create paragraphs use one or more lines consecutive text followed by one or more blank lines.    
6. Images link  
 ![Images link](https://www.elegantthemes.com/blog/wp-content/uploads/2017/07/Git-and-Github-Featured-Image.png) -  
 * To create an image link , use (!) exclamatory mark before the inline or reference link
7. **Lists**

* Bullet Lists
2. Numbered Lists
3.   * Mixed Lists
> Blockqoutes  
 
_THANK YOU_